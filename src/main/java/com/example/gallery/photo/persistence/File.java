package com.example.gallery.photo.persistence;


import com.example.gallery.base.Entity.DateAudit;
import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.base.config.SchemaConfig;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "files",schema = SchemaConfig.DB)

public class File extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "path",nullable = false)
    private String path;


    @Column(name = "width",columnDefinition = "INT(5)",nullable = false)
    private Integer width;

    @Column(name = "height",columnDefinition = "INT(5)",nullable = false)
    private Integer height;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id",referencedColumnName = "id")
    private Photo photo;

    public String getPath() {
        return PhotoConfig.BASE_PATH+path;
    }
}
