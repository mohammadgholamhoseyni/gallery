package com.example.gallery.order.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer> {
    @Query("select order from Order order where order.user.id=:id and order.status=:status")
    Order findOneByUserIdAndStatus(Integer id, OrderStatusEnum status);

    @Query("select order from Order order where order.user.id=:userId")
    List<Order> findAllByUserId(Integer userId);
}
