package com.example.gallery.category.service;



import com.example.gallery.category.controller.CategoryModel;
import com.example.gallery.category.controller.CategoryModelCreate;
import com.example.gallery.category.controller.CategoryModelUpdate;
import com.example.gallery.category.controller.CategoryTreeChildModel;
import com.example.gallery.category.persistence.Category;

import java.util.List;
import java.util.Set;

public interface CategoryService {

    CategoryModel create(CategoryModelCreate categoryModelCreate);


    CategoryModel show(Integer id);

    CategoryModel update(CategoryModelUpdate categoryModelUpdate);

    void delete(Integer id);

    List<CategoryModel> index();

    Set<Category> getSomeCategories(List<Integer> categories_id);

    List<CategoryTreeChildModel> tree();
}
