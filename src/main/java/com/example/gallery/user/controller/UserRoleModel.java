package com.example.gallery.user.controller;


import com.example.gallery.user.persistence.Role;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data

public class UserRoleModel {
    private String name;
}
