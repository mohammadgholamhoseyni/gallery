package com.example.gallery.photo.controller;

import com.example.gallery.category.controller.CategoryModel;
import com.example.gallery.category.persistence.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Lob;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
public class PhotoModelCreate {
    private String originalName;

    private Integer price;

    private List<Integer> categories_id;
}
