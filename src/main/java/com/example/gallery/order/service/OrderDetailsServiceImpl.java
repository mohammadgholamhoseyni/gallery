package com.example.gallery.order.service;

import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.order.controller.OrderDetailsFileModel;
import com.example.gallery.order.controller.OrderDetailsModel;
import com.example.gallery.order.controller.OrderModel;
import com.example.gallery.order.controller.TotalPriceModel;
import com.example.gallery.order.persistence.*;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.photo.service.PhotoServiceImpl;
import com.example.gallery.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier("OrderDetailsServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class OrderDetailsServiceImpl implements OrderDetailsService {

    private OrderDetailsRepository orderDetailsRepository;
    private UserService userService;

    @Autowired
    public OrderDetailsServiceImpl(OrderDetailsRepository orderDetailsRepository,UserService userService) {
        this.orderDetailsRepository = orderDetailsRepository;
        this.userService = userService;
    }

    @Override
    public OrderDetailsModel create(OrderModel orderModel,PhotoModel photoModel) {
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setCount(1);
        orderDetails.setPhoto(PhotoServiceImpl.convertModelToEntity(photoModel));
        orderDetails.setOrder(OrderServiceImpl.convertModelToEntity(orderModel));
        orderDetailsRepository.save(orderDetails);
        return convertEntityToModel(orderDetails);
    }

    @Override
    public List<OrderDetailsModel> getOpenOrderDetails() {
       return orderDetailsRepository.findAllByOrderStatus(OrderStatusEnum.OPENED,userService.getAuthUser().getId())
               .stream()
               .map(OrderDetailsServiceImpl::convertEntityToModel)
               .collect(Collectors.toList());
    }

    @Override
    public Boolean checkPhotoHasOrder(Integer id) {
        if (orderDetailsRepository.findAllByPhotoId(id).size()>0)
            return true;
        return false;
    }

    public static TotalPriceModel totalPrice(List<OrderDetailsModel> orderDetailsModelList) {
        Integer sum=0;
       for (int i =0;i<orderDetailsModelList.size();i++){
           sum+=orderDetailsModelList.get(i).getCount() * orderDetailsModelList.get(i).getPrice();
       }
       return new TotalPriceModel(sum);
    }

    public static OrderDetailsModel convertEntityToModel(OrderDetails orderDetails) {
        OrderDetailsModel orderDetailsModel = new OrderDetailsModel();
        orderDetailsModel.setId(orderDetails.getId());
        orderDetailsModel.setCount(orderDetails.getCount());
        orderDetailsModel.setOriginalName(orderDetails.getPhoto().getOriginalName());
        orderDetailsModel.setPrice(orderDetails.getPhoto().getPrice());
        if (orderDetails.getPhoto().getUser()!=null){
            orderDetailsModel.setPhoto_uploaded_by_user_id(orderDetails.getPhoto().getUser().getId());
            orderDetailsModel.setPhoto_uploaded_by_username(orderDetails.getPhoto().getUser().getUsername());
            if (orderDetails.getPhoto().getFiles()!=null){
                orderDetailsModel.setFiles(
                        orderDetails.getPhoto().getFiles().stream()
                                .map(OrderDetailsServiceImpl::convertEntityToFileModel)
                                .collect(Collectors.toList())

                );
            }
        }

        orderDetailsModel.setOrder_id(orderDetails.getOrder().getId());
        orderDetailsModel.setAmount(orderDetails.getOrder().getAmount());
        orderDetailsModel.setStatus(orderDetails.getOrder().getStatus());
        return orderDetailsModel;

    }

    public static OrderDetailsFileModel convertEntityToFileModel(File file) {
        OrderDetailsFileModel orderDetailsFileModel = new OrderDetailsFileModel();

            orderDetailsFileModel.setId(file.getId());
            orderDetailsFileModel.setWidth(file.getWidth());
            orderDetailsFileModel.setHeight(file.getHeight());
            orderDetailsFileModel.setPath(file.getPath());
        return orderDetailsFileModel;

    }

}
