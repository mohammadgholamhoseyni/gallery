package com.example.gallery.user.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.base.Responses.JwtResponse;
import com.example.gallery.base.config.RoleConfig;
import com.example.gallery.user.service.JwtTokenUtil;
import com.example.gallery.user.service.UserDetailsServiceImpl;
import com.example.gallery.user.service.UserService;
import com.example.gallery.user.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/")
public class UserController {

    private UserService userService;
    private JwtTokenUtil jwtTokenUtil;
    private UserDetailsService userDetailsService;

    @Autowired
    public UserController(UserServiceImpl userService, JwtTokenUtil jwtTokenUtil, UserDetailsServiceImpl userDetailsService) {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody LoginModel loginModel) throws Exception {

        userService.authenticate(loginModel.getUsername(), loginModel.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(loginModel.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new ApiResponse(true,new JwtResponse(token),"","", HttpStatus.OK));
    }

    @PostMapping(value = "/photographer-register")
    public ResponseEntity<?> createPhotographer(@Valid @RequestBody UserModelCreate userModelCreate) throws Exception {
        UserModel userModel = userService.create(userModelCreate, RoleConfig.ROLE_PHOTOGRAPHER);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"با موفقیت اضافه شدید.","", HttpStatus.OK));
    }

    @PostMapping(value = "/customer-register")
    public ResponseEntity<?> createCustomer(@Valid @RequestBody UserModelCreate userModelCreate) throws Exception {
        UserModel userModel = userService.create(userModelCreate, RoleConfig.ROLE_CUSTOMER);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"با موفقیت اضافه شدید.","", HttpStatus.OK));
    }
    @GetMapping(value = "user/show/{id}")
    public ResponseEntity<?> show(@PathVariable Integer id) throws Exception {
        UserModel userModel = userService.show(id);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"","", HttpStatus.OK));
    }
}
