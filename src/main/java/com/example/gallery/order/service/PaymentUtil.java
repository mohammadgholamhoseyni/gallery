package com.example.gallery.order.service;
import lombok.Data;
import java.io.*;
@Data
public class PaymentUtil {
    private String merchantID;
    private String amount;
    private String description;
    private String callbackURL;

    public PaymentUtil(Integer amount, Integer orderId) {
        this.merchantID = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
        this.amount = amount.toString();
        this.description = "سفارش عکس";
        this.callbackURL = "http://127.0.0.1:8080/payment-verify/" + orderId;
    }

    public Object doPayment() throws IOException {
       return null;
    }
}
