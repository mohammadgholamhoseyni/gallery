package com.example.gallery.photo.service;

import com.example.gallery.photo.controller.FileModel;
import com.example.gallery.photo.controller.FileModelCreate;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.controller.PhotoModelCreate;

import java.util.List;

public interface FileService {
    void create(List<FileModelCreate> fileModelCreate,PhotoModel photoModel);

    void delete(PhotoModel photoModel);

    List<FileModel> indexByAll(Integer pageNo);

    FileModel showByAll(Integer id) throws Throwable;

    void update(List<FileModelCreate> fileModelUpdateList, PhotoModel photoModel);
}
