package com.example.gallery.user.controller;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserModelCreate {

	@NotBlank(message = "لطفا نام کاربری را وارد نمایید")
	private String username;

	@NotBlank(message = "لطفا رمز عبور را وارد نمایید")
	private String password;

	@Email(message = "ایمیل صحیح نمی باشد")
	@NotBlank(message = "لطفا ایمیل را وارد نمایید")
	private String email;
}