package com.example.gallery.order.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails,Integer> {
    @Query("select orderDetails from OrderDetails orderDetails where orderDetails.order.status=:status and orderDetails.order.user.id=:userId")
    List<OrderDetails> findAllByOrderStatus(OrderStatusEnum status,Integer userId);

    @Query("select orderDet from OrderDetails orderDet where orderDet.photo.id=:photoId")
    List<OrderDetails> findAllByPhotoId(Integer photoId);
}
