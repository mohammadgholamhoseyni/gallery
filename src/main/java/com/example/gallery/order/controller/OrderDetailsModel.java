package com.example.gallery.order.controller;

import com.example.gallery.order.persistence.OrderStatusEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrderDetailsModel {
    private Integer id;
    private Integer count;
    private String originalName;
    private Integer price;
    private Integer photo_uploaded_by_user_id;
    private String photo_uploaded_by_username;
    private Integer order_id;
    private Integer amount;
    private OrderStatusEnum status;
    private List<OrderDetailsFileModel>files = new ArrayList<>();
}
