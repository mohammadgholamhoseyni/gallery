package com.example.gallery.photo.controller;

import com.example.gallery.category.persistence.Category;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.user.persistence.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
public class PhotoModel {
    private Integer id;
    private String originalName;
    private Integer price;
    private List<PhotoCategoryModel> categories = new ArrayList<>();

    private List<PhotoFileModel> files = new ArrayList<>();

    private Integer user_id;
    private String username;

}
