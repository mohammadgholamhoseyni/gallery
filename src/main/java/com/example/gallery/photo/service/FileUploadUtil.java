package com.example.gallery.photo.service;

import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.photo.controller.FileModelCreate;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.persistence.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


public class FileUploadUtil {

    private final List<FileModelCreate> fileModelCreateList;

    public FileUploadUtil() {
        this.fileModelCreateList = new ArrayList<>() ;
    }

    public List<FileModelCreate> uploadFile(String originalName, MultipartFile file) throws IOException {

        String fileName = fileNameGenerator(originalName, "original");

        Path uploadPath = Paths.get(PhotoConfig.UPLOAD_DIR);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = file.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);

            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

            BufferedImage bufferedImage = convertMultipartToBufferedImage(file);

            createFileModel(filePath, bufferedImage.getWidth(), bufferedImage.getHeight());

            uploadResizeFile(originalName, file, uploadPath);

            return fileModelCreateList;
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }

    }

    private void uploadResizeFile(String originalName, MultipartFile file, Path uploadPath) throws IOException {
        BufferedImage newBi = convertMultipartToBufferedImage(file);
        for (int i = 0; i < PhotoConfig.widths.length; i++) {
            String postFix = "_" + PhotoConfig.widths[i] + "_" + PhotoConfig.heights[i];
            String resizeFileName = fileNameGenerator(originalName, postFix);
            BufferedImage bufferedImage = resizeImage(newBi, PhotoConfig.widths[i], PhotoConfig.heights[i]);

            InputStream targetStream = convertBufferedImageToInputStream(bufferedImage, FilenameUtils.getExtension(file.getOriginalFilename()));
            Path resizeFilePath = uploadPath.resolve(resizeFileName);
            Files.copy(targetStream, resizeFilePath, StandardCopyOption.REPLACE_EXISTING);

            createFileModel(resizeFilePath, bufferedImage.getWidth(), bufferedImage.getHeight());
        }
    }

    private void createFileModel(Path filePath, Integer width, Integer height) {
        FileModelCreate fileModelCreate = new FileModelCreate(filePath.toString(), width, height);
        fileModelCreateList.add(fileModelCreate);
    }

    private InputStream convertBufferedImageToInputStream(BufferedImage bufferedImage, String formatName) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, formatName, baos);
        byte[] bytes = baos.toByteArray();
        InputStream targetStream = new ByteArrayInputStream(bytes);
        return targetStream;
    }

    private BufferedImage convertMultipartToBufferedImage(MultipartFile file) throws IOException {
        InputStream is = new ByteArrayInputStream(file.getBytes());
        BufferedImage newBi = ImageIO.read(is);
        return newBi;
    }

    private static String fileNameGenerator(String originalName, String postFix) {
        Instant instant = Instant.now();
        long timeStampMillis = instant.toEpochMilli();
        String extension = FilenameUtils.getExtension(originalName);
        String fileName = timeStampMillis + postFix + "." + extension;
        return fileName;
    }

    BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();
        return resizedImage;
    }

    public void deleteFile(PhotoModel photoModel) throws IOException {
        photoModel.getFiles().forEach(file->
        {
            try {
                FileUtils.touch(new java.io.File(removePreUrl(file.getPath())).toPath().toFile());
                FileUtils.forceDelete(FileUtils.getFile(removePreUrl(file.getPath())).toPath().toFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        );
    }
    public static String removePreUrl(String main){
        return main.replace(PhotoConfig.BASE_PATH,"");
    }
}
