package com.example.gallery.photo.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.order.service.OrderDetailsService;
import com.example.gallery.photo.service.FileService;
import com.example.gallery.photo.service.FileUploadUtil;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("photographer/photo")
@Validated
public class PhotoControllerPhotographer {

    private PhotoService photoService;
    private FileService fileService;
    private OrderDetailsService orderDetailsService;

    @Autowired
    public PhotoControllerPhotographer(PhotoService photoService, FileService fileService, OrderDetailsService orderDetailsService) {
        this.photoService = photoService;
        this.fileService = fileService;
        this.orderDetailsService = orderDetailsService;
    }

    @GetMapping(value = {"/index", "/index/"})
    public ResponseEntity<?> index() throws Exception {
        List<PhotoModel> photoModel = photoService.indexByPhotographer();
        return ResponseEntity.ok(new ApiResponse(true, photoModel, "", "", HttpStatus.OK));
    }

    @PostMapping(value = {"/create", "/create/"})
    public ResponseEntity<?> create(
          @RequestParam("price")   @NotBlank(message = "قیمت نمی تواند خالی باشد") String price,
          final @RequestParam("photo") MultipartFile file,
          @RequestParam("categories_id") @NotEmpty(message = "دسته بندی نمی تواند خالی باشد") List<Integer> categoriesId
    ) throws Exception {
        ValidatePhoto.checkFileSize(file);
        ValidatePhoto.chekFileType(file);
        String originalName = file.getOriginalFilename();
        FileUploadUtil fileUploadUtil = new FileUploadUtil();
        List<FileModelCreate> fileModelCreateList = fileUploadUtil.uploadFile(originalName, file);
        PhotoModelCreate photoModelCreate = new PhotoModelCreate(originalName, Integer.parseInt(price), categoriesId);
        PhotoModel photoModel = photoService.create(photoModelCreate);
        fileService.create(fileModelCreateList,photoModel);
        return ResponseEntity.ok(new ApiResponse(true, "", "عکس شما با موفقیت ایجاد شد", "", HttpStatus.OK));
    }

    @GetMapping(value = {"/show/{id}", "/show/{id}/"})
    public ResponseEntity<?> show(@PathVariable Integer id) throws Exception {
        photoService.checkPhotoIsOwn(id);
        PhotoModel photoModel = photoService.show(id);
        return ResponseEntity.ok(new ApiResponse(true, photoModel, "", "", HttpStatus.OK));
    }

    @PutMapping(value = {"/update", "/update/"})
    public ResponseEntity<?> update(@RequestParam("id") Integer id,
                                    @RequestParam("price") @Min(value = 0,message = "قیمت حداقل 0 می باشد") @NotBlank(message = "قیمت نمی تواند خالی باشد") String price,
                                    final @RequestParam("photo") MultipartFile file,
                                    @RequestParam("categories_id") List<Integer> categoriesId
    ) throws Exception {
        photoService.checkPhotoIsOwn(id);
        if (!file.isEmpty()){
            if (orderDetailsService.checkPhotoHasOrder(id))
                return ResponseEntity.ok(new ApiResponse(false, "", "این عکس را نمی شود ویرایش کرد", "", HttpStatus.OK));
            ValidatePhoto.checkFileSize(file);
            ValidatePhoto.chekFileType(file);
        }
        PhotoModel photoModel = photoService.show(id);
        String originalName = !file.isEmpty() ? file.getOriginalFilename() : photoModel.getOriginalName();
        PhotoModelUpdate photoModelUpdate = new PhotoModelUpdate(id, originalName, Integer.parseInt(price), categoriesId);
        photoService.update(photoModelUpdate);
        if (!file.isEmpty()) {
            FileUploadUtil fileUploadUtil = new FileUploadUtil();
            fileUploadUtil.deleteFile(photoModel);
            List<FileModelCreate> fileModelUpdateList=fileUploadUtil.uploadFile(originalName, file);
            fileService.update(fileModelUpdateList,photoModel);
        }

        return ResponseEntity.ok(new ApiResponse(true, "", "عکس مورد نظر با موفقیت ویرایش شد", "", HttpStatus.OK));
    }

    @DeleteMapping(value = {"/delete/{id}", "/delete/{id}/"})
    public ResponseEntity<?> delete(@PathVariable Integer id) throws Exception {
        photoService.checkPhotoIsOwn(id);
        if (orderDetailsService.checkPhotoHasOrder(id))
            return ResponseEntity.ok(new ApiResponse(false, "", "این عکس را نمی شود حذف کرد", "", HttpStatus.OK));
        PhotoModel photoModel = photoService.show(id);
        FileUploadUtil fileUploadUtil = new FileUploadUtil();
        fileUploadUtil.deleteFile(photoModel);
        fileService.delete(photoModel);
        photoService.delete(id);
        return ResponseEntity.ok(new ApiResponse(true, "", "عکس مورد نظر با موفقیت حذف گردید", "", HttpStatus.OK));
    }

}
