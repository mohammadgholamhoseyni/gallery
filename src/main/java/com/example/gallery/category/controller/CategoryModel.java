package com.example.gallery.category.controller;

import com.example.gallery.category.persistence.Category;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
public class CategoryModel {
    private Integer id;

    private String name;

    private CategoryParentModel parent;

    private List<CategoryChildModel> child;
}
