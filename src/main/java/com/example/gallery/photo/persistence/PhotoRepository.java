package com.example.gallery.photo.persistence;

import com.example.gallery.user.persistence.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends JpaRepository<Photo,Integer> {

    @Query("SELECT photos from Photo photos where photos.user.id=:id order by photos.id desc")
    List<Photo>findAllByUserIdOOrderByIdDesc(int id);


    List<Photo>findByOrderByIdDesc(Pageable pageable);

//    @Query("SELECT photos from Photo photos " +
//            "INNER JOIN OrderDetails orderdet on photos.id=orderdet.photo.id " +
//            "INNER JOIN Order orders on orderdet.order.id=orders.id " +
//            "where orders.status=com.example.gallery.order.persistence.OrderStatusEnum.ACCEPTED " +
//            "and orders.user.id=:userId")
@Query("SELECT  distinct photos from Photo photos JOIN photos.orderDetails orderDet " +
        "where orderDet.order.status = com.example.gallery.order.persistence.OrderStatusEnum.ACCEPTED " +
        "and orderDet.order.user.id=:userId")

    List<Photo>findAllBySuccessOrderStatus(Integer userId);

    @Query("select photo from Photo photo where photo.user.id=:userId")
    List<Photo> findAllByUserId(Integer userId);

    @Query("select photo from Photo photo join photo.categories category where category.id=:categoryId")
    List<Photo> findAllByCategoryId(Integer categoryId);

    @Query("select photo from Photo photo where photo.id=:photoId and photo.adminVerified=com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum.ACCEPTED")
    Optional<Photo> findByIdAndAdminVerified(Integer photoId);

}
