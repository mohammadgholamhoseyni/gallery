package com.example.gallery.category.controller;

import com.example.gallery.category.persistence.Category;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data

public class CategoryParentModel {
    private Integer id;

    private String name;
}
