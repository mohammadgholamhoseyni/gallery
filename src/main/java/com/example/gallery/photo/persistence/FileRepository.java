package com.example.gallery.photo.persistence;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends JpaRepository<File,Integer> {

    @Query("select file from File file where file.photo.adminVerified=com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum.ACCEPTED and file.width=:width and file.height=:height")
    List<File>findAllByWidthAndHeight(Pageable pageable, Integer width, Integer height);

    @Query("select file from File file where file.photo.adminVerified=com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum.ACCEPTED and file.id=:id and file.width=:width and file.height=:height")
    Optional<File> findByIdAndWidthAndHeight(Integer id, Integer width, Integer height);
}
