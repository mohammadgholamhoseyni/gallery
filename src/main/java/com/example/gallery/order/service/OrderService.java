package com.example.gallery.order.service;



import com.example.gallery.order.controller.OrderModel;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.order.persistence.OrderStatusEnum;
import com.example.gallery.photo.controller.PhotoModel;

public interface OrderService {


    OrderModel create(PhotoModel photoId);

    OrderModel checkPendingOrder();

    OrderModel checkOpenOrder();

    OrderModel updateAmountAndStatus(OrderModel orderModel);

    void changeStatus(Integer orderId, OrderStatusEnum orderStatusEnum);

    Boolean checkUserHasOrder();

    Boolean checkOtherUserHasOrder(Integer id);
}
