package com.example.gallery.user.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.order.service.OrderService;
import com.example.gallery.photo.service.PhotoService;
import com.example.gallery.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("photographer/user")
public class UserControllerPhotographer {

    private UserService userService;
    private PhotoService photoService;
    private OrderService orderService;

    @Autowired
    public UserControllerPhotographer(UserService userService, PhotoService photoService, OrderService orderService) {
        this.userService = userService;
        this.photoService = photoService;
        this.orderService = orderService;
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@Valid @RequestBody UserModelUpdate userModelUpdate) throws Exception {
        userService.checkUserIsOwn(userModelUpdate.getId());

        UserModel userModel = userService.update(userModelUpdate);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"با موفقیت ویرایش گردید","", HttpStatus.OK));
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) throws Exception {
        userService.checkUserIsOwn(id);
        if (photoService.checkUserHasPhoto())
            return ResponseEntity.ok(new ApiResponse(false,"","ابتدا عکس های آپلود شده ی خود را حذف نمایید","", HttpStatus.OK));
        if (orderService.checkUserHasOrder())
            return ResponseEntity.ok(new ApiResponse(false,"","شما سفارش ثبت شده ای دارید","", HttpStatus.OK));
        userService.delete(id);
        return ResponseEntity.ok(new ApiResponse(true,"","با موفقیت حذف شد","", HttpStatus.OK));
    }


}
