package com.example.gallery.photo.service;

import com.example.gallery.base.Exceptions.ResourceNotFoundException;
import com.example.gallery.base.config.PaginateConfig;
import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.category.service.CategoryService;
import com.example.gallery.photo.controller.*;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.photo.persistence.FileRepository;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.photo.persistence.PhotoRepository;
import com.example.gallery.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier("FileServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class FileServiceImpl implements FileService {
    private FileRepository fileRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public void create(List<FileModelCreate> fileModelCreateList, PhotoModel photoModel) {
        fileModelCreateList.forEach(value -> {
            File newFile = new File();
            newFile.setPath(value.getPath());
            newFile.setWidth(value.getWidth());
            newFile.setHeight(value.getHeight());
            newFile.setPhoto(PhotoServiceImpl.convertModelToEntity(photoModel));
            fileRepository.save(newFile);
        });
    }

    @Override
    public void delete(PhotoModel photoModel) {
        fileRepository.deleteAll(photoModel.getFiles().stream().map(FileServiceImpl::convertModelToEntity).collect(Collectors.toList()));
    }

    @Override
    public List<FileModel> indexByAll(Integer pageNo) {
        Pageable paging = PageRequest.of(pageNo, PaginateConfig.PAGE_SIZE, Sort.by(PaginateConfig.SORT_PAGE_BY));
        return fileRepository.
                findAllByWidthAndHeight(paging,PhotoConfig.widths[0], PhotoConfig.heights[0])
                .stream()
                .map(FileServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public FileModel showByAll(Integer id) throws Throwable {
        return convertEntityToModel(
                fileRepository
                        .findByIdAndWidthAndHeight(id, PhotoConfig.widths[0], PhotoConfig.heights[0])
                        .orElseThrow(() -> new ResourceNotFoundException("file", "id", id))
        );
    }

    @Override
    public void update(List<FileModelCreate> fileModelUpdateList, PhotoModel photoModel) {
        for (int i = 0; i < photoModel.getFiles().size(); i++) {
            File file = getSingleFile(photoModel.getFiles().get(i).getId());
            file.setHeight(fileModelUpdateList.get(i).getHeight());
            file.setWidth(fileModelUpdateList.get(i).getWidth());
            file.setPath(fileModelUpdateList.get(i).getPath());
            fileRepository.save(file);
        }
    }

    private File getSingleFile(Integer id) {
        return fileRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("file", "id", id));
    }

    public static FileModel convertEntityToModel(File file) {
        FileModel fileModel = new FileModel();
        fileModel.setId(file.getId());
        fileModel.setPath(file.getPath());
        fileModel.setWidth(file.getWidth());
        fileModel.setHeight(file.getHeight());
        fileModel.setHeight(file.getHeight());
        fileModel.setPhoto_id(file.getPhoto().getId());
        fileModel.setOriginalName(file.getPhoto().getOriginalName());
        fileModel.setPrice(file.getPhoto().getPrice());
        fileModel.setPhoto_user_id(file.getPhoto().getUser().getId());
        fileModel.setUsername(file.getPhoto().getUser().getUsername());
        if (file.getPhoto() != null) {
            fileModel.setCategories(
                    file.getPhoto().getCategories()
                            .stream()
                            .map(PhotoServiceImpl::convertCategoryEntityToModel)
                            .collect(Collectors.toList()));
        }
        return fileModel;
    }

    public static File convertModelToEntity(PhotoFileModel photoFileModel) {
        File file = new File();
        file.setId(photoFileModel.getId());
        file.setPath(photoFileModel.getPath());
        file.setWidth(photoFileModel.getWidth());
        file.setHeight(photoFileModel.getHeight());
        return file;
    }
}
