package com.example.gallery.user.controller;


import com.example.gallery.user.persistence.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class UserModel {
    private Integer id;
    private String username;
    @JsonIgnore
    private String password;
    private String email;

    private Collection<UserRoleModel> roles=new ArrayList<>();
}
