package com.example.gallery.category.controller;

import com.example.gallery.category.persistence.Category;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CategoryModelUpdate {
    @NotNull
    private Integer id;

    @NotBlank(message = "لطفا نام را وارد کنید")
    private String name;

    private Integer parent_id;

}
