package com.example.gallery.user.service;

import com.example.gallery.base.Exceptions.NotOwnException;
import com.example.gallery.base.Exceptions.ResourceNotFoundException;
import com.example.gallery.base.config.PaginateConfig;
import com.example.gallery.user.controller.*;
import com.example.gallery.user.persistence.Role;
import com.example.gallery.user.persistence.RoleRepository;
import com.example.gallery.user.persistence.User;
import com.example.gallery.user.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier("UserServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private PasswordEncoder bcryptEncoder;
    private AuthenticationManager authenticationManager;
    private RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder bcryptEncoder, AuthenticationManager authenticationManager, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.bcryptEncoder = bcryptEncoder;
        this.authenticationManager = authenticationManager;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<UserModel> index(Integer pageNo) {
        Pageable paging = PageRequest.of(pageNo, PaginateConfig.PAGE_SIZE, Sort.by(PaginateConfig.SORT_PAGE_BY));
        return   userRepository.findAll(paging)
                .stream()
                .map(UserServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    public UserModel create(UserModelCreate user, String role) {
        Role role1 = roleRepository.findRoleByName(role);
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        newUser.setActive(true);
        newUser.setRoles(Collections.singletonList(role1));
        userRepository.save(newUser);
        return convertEntityToModel(newUser);
    }

    public void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @Override
    public UserModel show(Integer id) {
        return convertEntityToModel(getUser(id));
    }

    @Override
    public UserModel update(UserModelUpdate userModelUpdate) {
        User user = getUser(userModelUpdate.getId());
        user.setPassword(bcryptEncoder.encode(userModelUpdate.getPassword()));
        user.setEmail(userModelUpdate.getEmail());
        userRepository.save(user);
        return convertEntityToModel(user);
    }

    @Override
    public void delete(Integer id) {
        User user = getUser(id);
        userRepository.delete(user);
    }

    @Override
    public void checkUserIsOwn(Integer id) {
        if (id != getAuthUser().getId())
            throw new NotOwnException("user");
    }

    @Override
    public User getAuthUser() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByUsername(userName);
    }

    public User getUser(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
    }

    public static UserModel convertEntityToModel(User user) {
        UserModel userModel =  new UserModel();
        userModel.setId(user.getId());
        userModel.setUsername(user.getUsername());
        userModel.setPassword(user.getPassword());
        userModel.setEmail(user.getEmail());
        if (user.getRoles()!=null){
            userModel.setRoles(user.getRoles().stream().map(UserServiceImpl::convertRoleEntityToUserRoleModel).collect(Collectors.toList()));
        }

        return userModel;
    }
    public static UserRoleModel convertRoleEntityToUserRoleModel(Role role) {
        UserRoleModel userRoleModel =  new UserRoleModel();
        userRoleModel.setName(role.getName());
        return userRoleModel;
    }

}
