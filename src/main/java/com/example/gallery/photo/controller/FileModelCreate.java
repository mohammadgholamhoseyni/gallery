package com.example.gallery.photo.controller;

import com.example.gallery.photo.persistence.Photo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FileModelCreate {
    private String path;

    private Integer width;
    private Integer height;
}
