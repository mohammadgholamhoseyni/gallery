package com.example.gallery.order.persistence;

import com.example.gallery.base.config.SchemaConfig;
import com.example.gallery.photo.persistence.Photo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "order_details",schema = SchemaConfig.DB)
public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "count")
    private Integer count;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id",referencedColumnName = "id")
    private Photo photo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id",referencedColumnName = "id")
    private Order order;
}
