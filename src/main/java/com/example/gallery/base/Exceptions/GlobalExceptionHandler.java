package com.example.gallery.base.Exceptions;

import com.example.gallery.base.Responses.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, ex.getMessage(), "", HttpStatus.NOT_FOUND);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(SomeResourceNotFoundException.class)
    public ResponseEntity<?> someResourceNotFoundException(SomeResourceNotFoundException ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, ex.getMessage(), "", HttpStatus.NOT_FOUND);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotOwnException.class)
    public ResponseEntity<?> notOwnException(NotOwnException ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, ex.getMessage(), "", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SizeFileException.class)
    public ResponseEntity<?> sizeFileException(SizeFileException ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, ex.getMessage(), "", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(TypeFileException.class)
    public ResponseEntity<?> typeFileException(TypeFileException ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, ex.getMessage(), "", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
        ApiResponse apiResponse = new ApiResponse(false, null, "", ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {

            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        ApiResponse apiResponse = new ApiResponse(false, null, "", errors, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Object>(apiResponse, HttpStatus.BAD_REQUEST);
    }

}
