package com.example.gallery.order.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.order.service.OrderDetailsService;
import com.example.gallery.order.service.OrderService;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/order")
public class OrderControllerAnyRole {
    private OrderService orderService;
    private PhotoService photoService;
    private OrderDetailsService orderDetailsService;

    @Autowired
    public OrderControllerAnyRole(OrderService orderService,PhotoService photoService,OrderDetailsService orderDetailsService) {
        this.orderService = orderService;
        this.photoService = photoService;
        this.orderDetailsService = orderDetailsService;
    }

    @PostMapping(value = {"/create/{photoId}","/create/{photoId}/"})
    public ResponseEntity<?> create(@PathVariable Integer photoId) throws Exception {
        OrderModel pending = orderService.checkPendingOrder();
        if (pending!=null){
            return ResponseEntity.ok(new ApiResponse(false,"","شما یک درخواست در حال انتظار دارید","", HttpStatus.OK));
        }
        PhotoModel photoModel = photoService.showOrderPhoto(photoId);
        OrderModel orderModel = orderService.checkOpenOrder();
        if (orderModel==null){
            orderModel = orderService.create(photoModel);
        }
        OrderDetailsModel orderDetailsModel = orderDetailsService.create(orderModel,photoModel);
        return ResponseEntity.ok(new ApiResponse(true,orderDetailsModel,"این عکس به سفارشات شما اضافه شد","", HttpStatus.OK));
    }
}
