package com.example.gallery.base.faktory;

import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.base.config.RoleConfig;
import com.example.gallery.category.persistence.Category;
import com.example.gallery.category.persistence.CategoryRepository;
import com.example.gallery.order.persistence.*;
import com.example.gallery.photo.persistence.*;
import com.example.gallery.user.persistence.Role;
import com.example.gallery.user.persistence.RoleRepository;
import com.example.gallery.user.persistence.User;
import com.example.gallery.user.persistence.UserRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component("DataLoader")
public class DataLoader implements CommandLineRunner {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;


    private void loadRoleData() {
        if (roleRepository.count() == 0) {
            Role role1 = new Role();
            role1.setName(RoleConfig.ROLE_ADMIN);

            Role role2 = new Role();
            role2.setName(RoleConfig.ROLE_PHOTOGRAPHER);

            Role role3 = new Role();
            role3.setName(RoleConfig.ROLE_CUSTOMER);

            roleRepository.save(role1);
            roleRepository.save(role2);
            roleRepository.save(role3);
        }
    }

    private void loadUserData() {
        if (userRepository.count() == 0) {
            List<Role> roles = new ArrayList<>();
            roles.add(roleRepository.findRoleByName(RoleConfig.ROLE_ADMIN));
            User user = new User();
            user.setUsername("mohammad");
            user.setPassword(bcryptEncoder.encode("456852"));
            user.setEmail("mohammad@gmail.com");
            user.setActive(true);
            user.setRoles(roles);
            userRepository.save(user);
        }
    }

    private void loadSomeUsersData() {
        if (userRepository.count() == 1) {
            List<Role> roles = new ArrayList<>();
            roles.add(roleRepository.findRoleByName(RoleConfig.ROLE_CUSTOMER));
            roles.add(roleRepository.findRoleByName(RoleConfig.ROLE_PHOTOGRAPHER));
            for (int i = 0; i < 100; i++) {
                List<Role> selectedRole = new ArrayList<>();
                User user = new User();
                Faker faker = new Faker();
                user.setUsername(faker.name().username());
                user.setPassword(bcryptEncoder.encode("456852"));
                user.setEmail(faker.internet().safeEmailAddress());
                user.setActive(true);
                selectedRole.add(roles.get(getRandomNumberInRange(0, roles.size() - 1)));
                user.setRoles(selectedRole);
                userRepository.save(user);
            }
        }
    }

    private void loadCategoryData() {
        if (categoryRepository.count() == 0) {
            for (int i = 0; i < 1000; i++) {
                Faker faker = new Faker();
                Category category = new Category();
                category.setName(faker.cat().name());
                categoryRepository.save(category);
            }
            for (int i = 0; i < 1000; i++) {
                Category category = categoryRepository.findById(getRandomNumberInRange(1, 1000)).get();
                Category parentCategory = categoryRepository.findById(getRandomNumberInRange(1, 200)).get();
                category.setParent(parentCategory);
                categoryRepository.save(category);
            }
        }
    }

    private void loadPhotoData() {
        if (photoRepository.count() == 0) {
            List<User> users = userRepository.findAllByRoleName(RoleConfig.ROLE_PHOTOGRAPHER);
            for (int i = 0; i < 2000; i++) {
                Faker faker = new Faker();
                Photo photo = new Photo();
                photo.setOriginalName(faker.funnyName().name());
                photo.setPrice(getRandomNumberInRange(1000, 100000));
                List<PhotoAdminVerifiedEnum> photoAdminVerifiedEnums = Collections.unmodifiableList(Arrays.asList(PhotoAdminVerifiedEnum.values()));
                photo.setAdminVerified(photoAdminVerifiedEnums.get(getRandomNumberInRange(0, photoAdminVerifiedEnums.size() - 1)));
                photo.setUser(users.get(getRandomNumberInRange(0, users.size() - 1)));
                List<Category> categories = new ArrayList<>();
                categories.add(categoryRepository.findById(getRandomNumberInRange(1, 1000)).get());
                categories.add(categoryRepository.findById(getRandomNumberInRange(1, 1000)).get());
                photo.setUser(users.get(getRandomNumberInRange(0, users.size() - 1)));
                photo.setCategories(categories);
                photoRepository.save(photo);

                File file = new File();
                file.setWidth(1000);
                file.setHeight(1000);
                file.setPath(faker.internet().image());
                file.setPhoto(photo);
                fileRepository.save(file);
                for (int j = 0; j < PhotoConfig.widths.length; j++) {
                    File newFile = new File();
                    newFile.setWidth(PhotoConfig.widths[j]);
                    newFile.setHeight(PhotoConfig.heights[j]);
                    newFile.setPath(faker.internet().image());
                    newFile.setPhoto(photo);
                    fileRepository.save(newFile);
                }
            }
        }
    }

    private void loadOrderData() {
        if (orderRepository.count() == 0) {
            List<User> users = userRepository.findAll();
            for (int i = 0; i < 4000; i++) {
                Order order = new Order();
                order.setAmount(getRandomNumberInRange(1000, 1000000));
                List<OrderStatusEnum> orderStatus = Collections.unmodifiableList(Arrays.asList(OrderStatusEnum.values()));
                order.setStatus(orderStatus.get(getRandomNumberInRange(2, orderStatus.size() - 1)));
                order.setUser(users.get(getRandomNumberInRange(0, users.size() - 1)));
                orderRepository.save(order);
            }
        }
    }

    private void loadOrderDetailsData() {
        if (orderDetailsRepository.count() == 0) {
            List<Order> orders = orderRepository.findAll();
            List<Photo> photos = photoRepository.findAll();
            for (int i = 0; i < 4000; i++) {
                OrderDetails orderDetails = new OrderDetails();
                orderDetails.setOrder(orders.get(i));
                orderDetails.setPhoto(photos.get(getRandomNumberInRange(0,photos.size()-1)));
                orderDetails.setCount(1);
                orderDetailsRepository.save(orderDetails);
            }
        }
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    @Override
    public void run(String... args) throws Exception {
        loadRoleData();
        loadUserData();
        loadSomeUsersData();
        loadCategoryData();
        loadPhotoData();
        loadOrderData();
        loadOrderDetailsData();
    }
}
