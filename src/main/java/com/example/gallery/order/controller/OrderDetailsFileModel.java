package com.example.gallery.order.controller;

import lombok.Data;

import java.util.List;

@Data
public class OrderDetailsFileModel {
    private Integer id;
    private Integer width;
    private Integer height;
    private String path;
}
