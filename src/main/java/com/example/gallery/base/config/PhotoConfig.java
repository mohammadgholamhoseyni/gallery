package com.example.gallery.base.config;

public class PhotoConfig {
    public static  Integer [] widths = {200};
    public static  Integer [] heights = {200};

    public static final String BASE_PATH = "file:///E:/projects/Spring/gallery/";
    public static final String UPLOAD_DIR = "src/main/resources/static/upload/";
}
