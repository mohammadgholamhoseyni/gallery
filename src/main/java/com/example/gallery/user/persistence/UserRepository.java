package com.example.gallery.user.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query("select user.roles from User user where user.id=:id")
	List<Role> findAllRoleNameByUserId(Integer id);

	User findByUsername(String username);

	@Query("select user from User user inner join user.roles role where role.name=:roleName")
	List<User>findAllByRoleName(String roleName);

}