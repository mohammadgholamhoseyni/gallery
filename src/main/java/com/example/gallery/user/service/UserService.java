package com.example.gallery.user.service;

import com.example.gallery.user.controller.UserModel;
import com.example.gallery.user.controller.UserModelCreate;
import com.example.gallery.user.controller.UserModelUpdate;
import com.example.gallery.user.persistence.User;

import java.util.List;

public interface UserService {

    UserModel create(UserModelCreate userModelCreate, String role);

    void authenticate(String username, String password) throws Exception;

    UserModel show(Integer id);

    UserModel update(UserModelUpdate userModelUpdate);

    void delete(Integer id);

    List<UserModel> index(Integer pageNo);

    User getAuthUser();

    void checkUserIsOwn(Integer id);
}
