package com.example.gallery.order.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class OrderDetailsResponseIndex {
    private List<OrderDetailsModel>orderDetailsModelList= new ArrayList<>();
    private TotalPriceModel totalPriceModel;
}
