package com.example.gallery.user.persistence;

import com.example.gallery.base.Entity.DateAudit;
import com.example.gallery.base.config.SchemaConfig;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.photo.persistence.Photo;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collection;


@Entity
@Data
@Table(name = "users",schema = SchemaConfig.DB)
public class User extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username",unique = true,nullable = false)
    private String username;

    @Column(name = "password",nullable = false)
    @JsonIgnore
    private String password;

    @Column(name = "email",unique = true,nullable = false)
    @Email
    private String email;

    @Column(name = "is_active")
    private boolean isActive;


    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Collection<Photo> photos;


    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Collection<Order> orders;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "id")
    )
    private Collection<Role> roles;

}


