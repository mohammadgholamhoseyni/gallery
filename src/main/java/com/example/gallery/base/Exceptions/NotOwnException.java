package com.example.gallery.base.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotOwnException extends RuntimeException{
    private static final Long serialVersionUID = 3L;
    private String resourceName;

    public NotOwnException(String resourceName) {
        super(String.format("this %s is not for you", resourceName));
        this.resourceName = resourceName;
    }

    public static Long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }
}
