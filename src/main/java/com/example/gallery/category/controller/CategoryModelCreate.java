package com.example.gallery.category.controller;

import com.example.gallery.category.persistence.Category;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryModelCreate {
    @NotBlank(message = "لطفا نام را وارد کنید")
    private String name;

    private Integer parent_id;

}
