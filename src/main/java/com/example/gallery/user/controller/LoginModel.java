package com.example.gallery.user.controller;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class LoginModel implements Serializable {

	private static final Long serialVersionUID = 5926468683005150707L;

	@NotBlank(message = "نام کاربری را وارد کنید")
	private String username;

	@NotBlank(message = "رمزعبور را وارد کنید")
	private String password;
}