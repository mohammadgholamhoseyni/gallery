package com.example.gallery.base.config;

public class RoleConfig {
    public static final String ROLE_ADMIN="ADMIN";
    public static final String ROLE_PHOTOGRAPHER="PHOTOGRAPHER";
    public static final String ROLE_CUSTOMER="CUSTOMER";
}
