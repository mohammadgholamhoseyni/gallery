package com.example.gallery.photo.controller;

import com.example.gallery.category.persistence.Category;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.user.persistence.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data


public class PhotoCategoryModel {
    private Integer id;
    private String name;

}
