package com.example.gallery.photo.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.photo.service.FileUploadUtil;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("photos")
@Validated
public class PhotoControllerAnyRole {

    private PhotoService photoService;

    @Autowired
    public PhotoControllerAnyRole(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping(value = {"/bought", "/bought/"})
    public ResponseEntity<?> boughtPhotos() throws Exception {
        List<PhotoModel> photoModel = photoService.boughtPhotos();
        return ResponseEntity.ok(new ApiResponse(true, photoModel, "", "", HttpStatus.OK));
    }

}
