package com.example.gallery.base.Responses;

import org.springframework.http.HttpStatus;

public class ApiResponse {
    private boolean status;
    private Object data;
    private Object message;
    private Object error;
    private HttpStatus statusCode;

    public ApiResponse(boolean status, Object data, Object message, Object error, HttpStatus statusCode) {
        this.status = status;
        this.data = data;
        this.message = message;
        this.error = error;
        this.statusCode = statusCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }
}
