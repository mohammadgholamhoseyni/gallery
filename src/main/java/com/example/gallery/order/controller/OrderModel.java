package com.example.gallery.order.controller;

import com.example.gallery.order.persistence.OrderStatusEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrderModel {
    private Integer id;
    private Integer amount;
    private OrderStatusEnum status;
    private Integer user_id;
    private String username;
    private List<OrderDetailsModel>orderDetailsModelList = new ArrayList<>();
}
