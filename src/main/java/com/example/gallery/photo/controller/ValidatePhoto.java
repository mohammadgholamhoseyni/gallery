package com.example.gallery.photo.controller;

import com.example.gallery.base.Exceptions.SizeFileException;
import com.example.gallery.base.Exceptions.TypeFileException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

public class ValidatePhoto {
    public static void checkFileSize(MultipartFile file)throws Exception{
        if (file.getSize()>10000000)
           throw  new SizeFileException();
    }
    public static void chekFileType(MultipartFile file)throws Exception{
        if (!(FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase().equals("jpg")||
                FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase().equals("jpeg")||
                FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase().equals("png")))
            throw new TypeFileException();
    }
}
