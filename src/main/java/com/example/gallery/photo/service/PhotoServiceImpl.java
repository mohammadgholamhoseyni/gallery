package com.example.gallery.photo.service;

import com.example.gallery.base.Exceptions.NotOwnException;
import com.example.gallery.base.Exceptions.ResourceNotFoundException;
import com.example.gallery.base.Exceptions.SomeResourceNotFoundException;
import com.example.gallery.base.config.PaginateConfig;
import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.category.controller.CategoryModel;
import com.example.gallery.category.persistence.Category;
import com.example.gallery.category.persistence.CategoryRepository;
import com.example.gallery.category.service.CategoryService;
import com.example.gallery.photo.controller.*;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum;
import com.example.gallery.photo.persistence.PhotoRepository;
import com.example.gallery.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("PhotoServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class PhotoServiceImpl implements PhotoService {

    private PhotoRepository photoRepository;
    private UserService userService;
    private CategoryService categoryService;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository, UserService userService, CategoryService categoryService) {
        this.photoRepository = photoRepository;
        this.userService = userService;
        this.categoryService = categoryService;
    }

    @Override
    public List<PhotoModel> indexByPhotographer() {

        return photoRepository.findAllByUserIdOOrderByIdDesc(userService.getAuthUser().getId())
                .stream().map(PhotoServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void changeAdminVerified(Integer id, PhotoAdminVerifiedEnum status) {
        Photo photo = getSinglePhoto(id);
        photo.setAdminVerified(status);
        photoRepository.save(photo);
    }

    @Override
    public List<PhotoModel> indexByAdmin(Integer pageNo) {
        Pageable paging = PageRequest.of(pageNo, PaginateConfig.PAGE_SIZE, Sort.by(PaginateConfig.SORT_PAGE_BY));
        return photoRepository.findByOrderByIdDesc(paging)
                .stream().map(PhotoServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<PhotoModel> boughtPhotos() {
        return photoRepository.findAllBySuccessOrderStatus(userService.getAuthUser().getId())
                .stream()
                .map(PhotoServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean checkUserHasPhoto() {
        if (photoRepository.findAllByUserId(userService.getAuthUser().getId()).size()>0)
            return true;
        return false;
    }

    @Override
    public Boolean checkOtherUserHasPhoto(Integer id) {
        if (photoRepository.findAllByUserId(id).size()>0)
            return true;
        return false;
    }

    @Override
    public Boolean checkCategoryHasPhoto(Integer id) {
        if (photoRepository.findAllByCategoryId(id).size()>0)
            return true;
        return false;
    }

    @Override
    public PhotoModel showOrderPhoto(Integer photoId) {
        return convertEntityToModel(photoRepository.findByIdAndAdminVerified(photoId).orElseThrow(()->new ResourceNotFoundException("photo","id",photoId)));
    }

    @Override
    public PhotoModel create(PhotoModelCreate photoModelCreate) throws Exception {
        Photo newPhoto = new Photo();
        newPhoto.setOriginalName(photoModelCreate.getOriginalName());
        newPhoto.setPrice(photoModelCreate.getPrice());
        newPhoto.setUser(userService.getAuthUser());
        Set<Category> categories = categoryService.getSomeCategories(photoModelCreate.getCategories_id());
        checkCategoriesId(photoModelCreate.getCategories_id(), categories);
        newPhoto.setCategories(categories);
        photoRepository.save(newPhoto);
        return convertEntityToModel(newPhoto);
    }

    @Override
    public PhotoModel show(Integer id){
        return convertEntityToModel(getSinglePhoto(id));
    }

    @Override
    public PhotoModel update(PhotoModelUpdate photoModelUpdate) throws Exception {
        Photo photo = getSinglePhoto(photoModelUpdate.getId());
        photo.setOriginalName(photoModelUpdate.getOriginalName());
        photo.setPrice(photoModelUpdate.getPrice());
        photo.setUser(userService.getAuthUser());
        Set<Category> categories = categoryService.getSomeCategories(photoModelUpdate.getCategories_id());
        checkCategoriesId(photoModelUpdate.getCategories_id(), categories);
        photo.setCategories(categories);
        photoRepository.save(photo);
        return convertEntityToModel(photo);
    }

    @Override
    public void delete(Integer id) {
        Photo photo = getSinglePhoto(id);
        photoRepository.delete(photo);
    }

    @Override
    public void checkPhotoIsOwn(Integer id) {
        Photo photo = getSinglePhoto(id);
        if (userService.getAuthUser().getId() != photo.getUser().getId())
            throw new NotOwnException("photo");
    }


    public Photo getSinglePhoto(Integer id) {
        return photoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("photo", "id", id));
    }

    public void checkCategoriesId(List<Integer> categories_id, Set<Category> categories) throws Exception {
        if (categories_id.size() != categories.size())
            throw new SomeResourceNotFoundException("category");
    }

    public static PhotoModel convertEntityToModel(Photo photo) {
        PhotoModel photoModel = new PhotoModel();
        photoModel.setId(photo.getId());
        photoModel.setOriginalName(photo.getOriginalName());
        photoModel.setPrice(photo.getPrice());
        photoModel.setUser_id(photo.getUser().getId());
        photoModel.setUsername(photo.getUser().getUsername());
        if (photo.getCategories()!=null) {
            photoModel.setCategories(
                    photo.getCategories()
                            .stream()
                            .map(PhotoServiceImpl::convertCategoryEntityToModel)
                            .collect(Collectors.toList()));
        }
        if ( photo.getFiles()!=null) {
            photoModel.setFiles(
                    photo.getFiles()
                            .stream()
                            .map(PhotoServiceImpl::convertFileEntityToModel)
                            .collect(Collectors.toList()));
        }
        return photoModel;

    }

    public static Photo convertModelToEntity(PhotoModel photoModel) {
        Photo photo = new Photo();
        photo.setId(photoModel.getId());
        photo.setOriginalName(photoModel.getOriginalName());
        photo.setPrice(photoModel.getPrice());
        return photo;
    }

    public static PhotoCategoryModel convertCategoryEntityToModel(Category category) {
        PhotoCategoryModel photoCategoryModel = new PhotoCategoryModel();
        photoCategoryModel.setId(category.getId());
        photoCategoryModel.setName(category.getName());
        return photoCategoryModel;
    }

    public static PhotoFileModel convertFileEntityToModel(File file) {
        PhotoFileModel photoFileModel = new PhotoFileModel();
        photoFileModel.setId(file.getId());
        photoFileModel.setPath(file.getPath());
        photoFileModel.setWidth(file.getWidth());
        photoFileModel.setHeight(file.getHeight());
        return photoFileModel;
    }
}
