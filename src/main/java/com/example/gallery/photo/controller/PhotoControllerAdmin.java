package com.example.gallery.photo.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin/photo")
public class PhotoControllerAdmin {

    private PhotoService photoService;

    @Autowired
    public PhotoControllerAdmin(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping(value = {"/index", "/index/"})
    public ResponseEntity<?> index(@RequestParam(defaultValue = "0") Integer pageNo) throws Exception {
        List<PhotoModel> photoModel = photoService.indexByAdmin(pageNo);
        return ResponseEntity.ok(new ApiResponse(true, photoModel, "", "", HttpStatus.OK));
    }

    @GetMapping(value = {"/show/{id}", "/show/{id}/"})
    public ResponseEntity<?> show(@PathVariable Integer id) throws Exception {
        PhotoModel photoModel = photoService.show(id);
        return ResponseEntity.ok(new ApiResponse(true, photoModel, "", "", HttpStatus.OK));
    }

    @PutMapping(value = {"/accept/{id}", "/accept/{id}/"})
    public ResponseEntity<?> accept(@PathVariable Integer id) throws Throwable {
        photoService.changeAdminVerified(id, PhotoAdminVerifiedEnum.ACCEPTED);
        return ResponseEntity.ok(new ApiResponse(true, "", "عکس مورد نظر تایید شد", "", HttpStatus.OK));
    }

    @PutMapping(value = {"/reject/{id}", "/reject/{id}/"})
    public ResponseEntity<?> reject(@PathVariable Integer id) throws Throwable {
        photoService.changeAdminVerified(id,PhotoAdminVerifiedEnum.REJECTED);
        return ResponseEntity.ok(new ApiResponse(true, "", "عکس مورد نظر رد شد", "", HttpStatus.OK));
    }

}
