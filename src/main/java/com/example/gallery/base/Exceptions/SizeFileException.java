package com.example.gallery.base.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SizeFileException extends RuntimeException{
    private static final Long serialVersionUID = 3L;

    public SizeFileException() {
        super(String.format("حداکثر حجم  فایل 10 مگابایت  می باشد"));
    }
}
