package com.example.gallery.photo.service;

import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.controller.PhotoModelCreate;
import com.example.gallery.photo.controller.PhotoModelUpdate;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.photo.persistence.PhotoAdminVerifiedEnum;

import java.util.List;

public interface PhotoService {
    PhotoModel create(PhotoModelCreate photoModelCreate) throws Exception;

    PhotoModel show(Integer id) throws Exception;

    PhotoModel update(PhotoModelUpdate photoModelUpdate) throws Exception;

    void delete(Integer id);

    void checkPhotoIsOwn(Integer id);

    List<PhotoModel> indexByPhotographer();

    void changeAdminVerified(Integer id, PhotoAdminVerifiedEnum status);

    List<PhotoModel> indexByAdmin(Integer pageNo);

    List<PhotoModel> boughtPhotos();

    Boolean checkUserHasPhoto();

    Boolean checkOtherUserHasPhoto(Integer id);

    Boolean checkCategoryHasPhoto(Integer id);

    PhotoModel showOrderPhoto(Integer photoId);
}
