package com.example.gallery.photo.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.photo.service.FileService;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/photo")
public class FileController {

    private FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(value = {"/index", "/index/"})
    public ResponseEntity<?> index(@RequestParam(defaultValue = "0") Integer pageNo) throws Exception {
        List<FileModel> fileModels = fileService.indexByAll(pageNo);
        return ResponseEntity.ok(new ApiResponse(true, fileModels, "", "", HttpStatus.OK));
    }


    @GetMapping(value = {"/show/{id}", "/show/{id}/"})
    public ResponseEntity<?> show(@PathVariable Integer id) throws Throwable {
        FileModel fileModel = fileService.showByAll(id);
        return ResponseEntity.ok(new ApiResponse(true, fileModel, "", "", HttpStatus.OK));
    }

}
