package com.example.gallery.order.persistence;

public enum OrderStatusEnum {
    OPENED,PENDING,ACCEPTED,REJECTED
}
