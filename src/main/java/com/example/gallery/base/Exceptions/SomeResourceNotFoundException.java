package com.example.gallery.base.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SomeResourceNotFoundException extends RuntimeException{
    private static final Long serialVersionUID = 2L;
    private String resourceName;

    public SomeResourceNotFoundException(String resourceName) {
        super(String.format("some %s not found", resourceName));
        this.resourceName = resourceName;
    }

    public static Long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }
}
