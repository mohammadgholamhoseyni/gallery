package com.example.gallery.photo.controller;

import com.example.gallery.photo.persistence.Photo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
public class FilePhotoModel {
    private Integer id;
    private String originalName;
    private Integer price;
    private List<PhotoCategoryModel> categories;

    private Integer user_id;
    private String username;

}
