package com.example.gallery.order.service;


import com.example.gallery.order.persistence.PaymentStatusEnum;

public interface PaymentService {

    void create(Integer orderId, PaymentStatusEnum paymentStatusEnum);
}
