package com.example.gallery.photo.controller;

import com.example.gallery.category.persistence.Category;
import com.example.gallery.photo.persistence.File;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.user.persistence.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;
import java.util.Set;

@Data

public class FileModel {
    private Integer id;
    private String path;

    private Integer width;

    private Integer height;

    private Integer Photo_id;
    private String originalName;
    private Integer price;

    private Integer photo_user_id;
    private String username;

    private Collection<PhotoCategoryModel> categories;

}
