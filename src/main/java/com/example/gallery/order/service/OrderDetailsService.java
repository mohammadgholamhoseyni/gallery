package com.example.gallery.order.service;



import com.example.gallery.order.controller.OrderDetailsModel;
import com.example.gallery.order.controller.OrderModel;
import com.example.gallery.order.controller.TotalPriceModel;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.photo.controller.PhotoModel;

import java.util.List;

public interface OrderDetailsService {


    OrderDetailsModel create(OrderModel orderModel,PhotoModel photoModel);


    List<OrderDetailsModel> getOpenOrderDetails();

    Boolean checkPhotoHasOrder(Integer id);
}
