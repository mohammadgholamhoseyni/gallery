package com.example.gallery.category.service;

import com.example.gallery.base.Exceptions.ResourceNotFoundException;

import com.example.gallery.category.controller.*;
import com.example.gallery.category.persistence.Category;
import com.example.gallery.category.persistence.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Qualifier("CategoryServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryModel> index() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }


    public CategoryModel create(CategoryModelCreate category) {

        Category newCategory = new Category();
        newCategory.setName(category.getName());
        if (category.getParent_id() != null) {
            newCategory.setParent(getCategory(category.getParent_id()));
        }
        categoryRepository.save(newCategory);
        return convertEntityToModel(newCategory);
    }

    @Override
    public CategoryModel show(Integer id) {
        return convertEntityToModel(getCategory(id));
    }

    @Override
    public CategoryModel update(CategoryModelUpdate categoryModelUpdate) {
        Category category = getCategory(categoryModelUpdate.getId());
        category.setName(categoryModelUpdate.getName());
        if (categoryModelUpdate.getParent_id() != null) {
            category.setParent(getCategory(categoryModelUpdate.getParent_id()));
        }else{
            category.setParent(null);
        }
        categoryRepository.save(category);
        return convertEntityToModel(category);
    }

    @Override
    public void delete(Integer id) {
        Category category = getCategory(id);
        categoryRepository.delete(category);
    }
    @Override
    public Set<Category> getSomeCategories(List<Integer> categories_id) {
        return new HashSet<>(categoryRepository.findAllById(categories_id));
    }

    public Category getCategory(Integer id) {
        return categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("category", "id", id));
    }

    @Override
    public List<CategoryTreeChildModel> tree() {
        return categoryRepository.findAllByParentIsNull().stream()
                .map(CategoryServiceImpl::convertEntityToTreeModel)
                .collect(Collectors.toList());
    }

    public static CategoryModel convertEntityToModel(Category category) {
        CategoryModel categoryModel =new CategoryModel();
        categoryModel.setId(category.getId());
        categoryModel.setName(category.getName());
        if (category.getParent()!=null){
            categoryModel.setParent(convertEntityToParentModel(category.getParent()));
        }
        if (category.getChildren()!=null){
            categoryModel.setChild(category.getChildren().stream().map(CategoryServiceImpl::convertEntityToChildModel).collect(Collectors.toList()));
        }

        return categoryModel;
    }
    public static CategoryParentModel convertEntityToParentModel(Category category) {
        CategoryParentModel categoryParentModel =new CategoryParentModel();
        categoryParentModel.setId(category.getId());
        categoryParentModel.setName(category.getName());
        return categoryParentModel;
    }
    public static CategoryChildModel convertEntityToChildModel(Category category) {
        CategoryChildModel categoryChildModel =new CategoryChildModel();
        categoryChildModel.setId(category.getId());
        categoryChildModel.setName(category.getName());
        return categoryChildModel;
    }
    public static CategoryTreeChildModel convertEntityToTreeModel(Category category) {
        CategoryTreeChildModel categoryTreeChildModel =new CategoryTreeChildModel();
        categoryTreeChildModel.setId(category.getId());
        categoryTreeChildModel.setName(category.getName());
        categoryTreeChildModel.setChild(category.getChildren()
                .stream()
                .map(CategoryServiceImpl::convertEntityToTreeModel)
                .collect(Collectors.toList()));
        return categoryTreeChildModel;
    }
}
