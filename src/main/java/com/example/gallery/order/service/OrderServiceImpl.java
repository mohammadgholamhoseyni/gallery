package com.example.gallery.order.service;

import com.example.gallery.base.Exceptions.NotOwnException;
import com.example.gallery.base.Exceptions.ResourceNotFoundException;
import com.example.gallery.base.Exceptions.SizeFileException;
import com.example.gallery.order.controller.OrderModel;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.order.persistence.OrderRepository;
import com.example.gallery.order.persistence.OrderStatusEnum;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.photo.service.PhotoServiceImpl;
import com.example.gallery.user.service.UserService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

@Service
@Qualifier("OrderServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private UserService userService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, UserService userService) {
        this.orderRepository = orderRepository;
        this.userService = userService;
    }

    @Override
    public OrderModel create(PhotoModel photoModel) {
        Photo photo = PhotoServiceImpl.convertModelToEntity(photoModel);
        Order newOrder = new Order();
        newOrder.setAmount(photo.getPrice());
        newOrder.setUser(userService.getAuthUser());
        orderRepository.save(newOrder);
        return convertEntityToModel(newOrder);
    }

    @Override
    public OrderModel checkPendingOrder() {
        Order order = orderRepository.findOneByUserIdAndStatus(userService.getAuthUser().getId(), OrderStatusEnum.PENDING);
        if (order != null)
            return convertEntityToModel(order);
        return null;
    }

    @Override
    public OrderModel checkOpenOrder() {
        Order order = orderRepository.findOneByUserIdAndStatus(userService.getAuthUser().getId(), OrderStatusEnum.OPENED);
        if (order != null)
            return convertEntityToModel(order);
        return null;
    }

    @Override
    public OrderModel updateAmountAndStatus(OrderModel orderModel) {
        Order order = getSingleOrder(orderModel.getId());
        order.setStatus(OrderStatusEnum.PENDING);
        order.setAmount(OrderDetailsServiceImpl.totalPrice(orderModel.getOrderDetailsModelList()).getAmount());
        orderRepository.save(order);
        return convertEntityToModel(order);
    }

    @Override
    public void changeStatus(Integer orderId, OrderStatusEnum status) {
        Order order = getSingleOrder(orderId);
        order.setStatus(status);
        orderRepository.save(order);
    }
    public Order getSingleOrder(Integer id){
        return orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("order", "id", id));
    }

    @Override
    public Boolean checkUserHasOrder() {
        if (orderRepository.findAllByUserId(userService.getAuthUser().getId()).size()>0)
            return true;
        return false;
    }

    @Override
    public Boolean checkOtherUserHasOrder(Integer id) {
        if (orderRepository.findAllByUserId(id).size()>0)
            return true;
        return false;
    }

    public static OrderModel convertEntityToModel(Order order) {
        OrderModel orderModel = new OrderModel();
        orderModel.setId(order.getId());
        orderModel.setAmount(order.getAmount());
        orderModel.setStatus(order.getStatus());
        orderModel.setUser_id(order.getUser().getId());
        orderModel.setUsername(order.getUser().getUsername());
        if (order.getOrderDetails() != null) {
            orderModel.setOrderDetailsModelList(
                    order.getOrderDetails()
                            .stream()
                            .map(OrderDetailsServiceImpl::convertEntityToModel)
                            .collect(Collectors.toList()));
        }
        return orderModel;

    }

    public static Order convertModelToEntity(OrderModel orderModel) {
        Order order = new Order();
        order.setId(orderModel.getId());
        order.setAmount(orderModel.getAmount());
        order.setStatus(orderModel.getStatus());
        return order;
    }

}
