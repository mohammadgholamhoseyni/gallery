package com.example.gallery.user.controller;

import com.example.gallery.user.persistence.Role;
import com.example.gallery.user.persistence.User;
import com.example.gallery.user.service.UserServiceImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class MyUserDetails implements UserDetails {
    private UserModel userModel;

    public MyUserDetails(UserModel userModel) {
        this.userModel = userModel;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<UserRoleModel> roles = userModel.getRoles();
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        for (UserRoleModel role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return authorities;
    }
    @Override
    public String getPassword() {
        return userModel.getPassword();
    }

    @Override
    public String getUsername() {
        return userModel.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
