package com.example.gallery.user.controller;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UserModelUpdate {
	@NotNull
	private Integer id;

	@NotBlank(message = "لطفا رمز عبور را وارد نمایید")
	private String password;

	@Email(message = "ایمیل صحیح نمی باشد")
	@NotBlank(message = "لطفا ایمیل را وارد نمایید")
	private String email;
}