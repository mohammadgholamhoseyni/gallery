package com.example.gallery.base.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TypeFileException extends RuntimeException{
    private static final Long serialVersionUID = 3L;

    public TypeFileException() {
        super(String.format("فرمت فایل قابل قبول نیست"));
    }
}
