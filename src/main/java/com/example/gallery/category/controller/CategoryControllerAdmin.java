package com.example.gallery.category.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.category.service.CategoryService;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("admin/category")
public class CategoryControllerAdmin {

    private CategoryService categoryService;
    private PhotoService photoService;

    @Autowired
    public CategoryControllerAdmin(CategoryService categoryService, PhotoService photoService) {
        this.categoryService = categoryService;
        this.photoService = photoService;
    }

    @GetMapping(value = "/index")
    public ResponseEntity<?> index() throws Exception {
        List<CategoryModel> categoryModels = categoryService.index();
        return ResponseEntity.ok(new ApiResponse(true,categoryModels,"","", HttpStatus.OK));
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@Valid @RequestBody CategoryModelCreate categoryModelCreate) throws Exception {
        CategoryModel categoryModel = categoryService.create(categoryModelCreate);
        return ResponseEntity.ok(new ApiResponse(true,categoryModel,"با موفقیت اضاغه شد.","", HttpStatus.OK));
    }

    @GetMapping(value = "/show/{id}")
    public ResponseEntity<?> show(@PathVariable Integer id) throws Exception {
        CategoryModel categoryModel = categoryService.show(id);
        return ResponseEntity.ok(new ApiResponse(true,categoryModel,"","", HttpStatus.OK));
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@Valid @RequestBody CategoryModelUpdate categoryModelUpdate) throws Exception {
        CategoryModel categoryModel = categoryService.update(categoryModelUpdate);
        return ResponseEntity.ok(new ApiResponse(true,categoryModel,"با موفقیت ویرایش گردید","", HttpStatus.OK));
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) throws Exception {
        if (photoService.checkCategoryHasPhoto(id))
            return ResponseEntity.ok(new ApiResponse(false,"","این دسته بندی عکس دارد","", HttpStatus.OK));
        if (categoryService.show(id).getChild().size()>0)
            return ResponseEntity.ok(new ApiResponse(false,"","این دسته بندی زیرشاخه دارد ابتدا آن هارا حذف کنید","", HttpStatus.OK));
        categoryService.delete(id);
        return ResponseEntity.ok(new ApiResponse(true,"","با موفقیت حذف شد","", HttpStatus.OK));
    }
}
