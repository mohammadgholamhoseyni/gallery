package com.example.gallery.photo.persistence;

import com.example.gallery.base.Entity.DateAudit;
import com.example.gallery.base.config.SchemaConfig;
import com.example.gallery.category.persistence.Category;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.order.persistence.OrderDetails;
import com.example.gallery.user.persistence.User;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "photos",schema = SchemaConfig.DB)
public class Photo extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "original_name",nullable = false)
    private String originalName;

    @Column(name = "price",nullable = false)
    private Integer price;

    @Column(name = "admin_verified")
    @Enumerated(EnumType.ORDINAL)
    private PhotoAdminVerifiedEnum adminVerified=PhotoAdminVerifiedEnum.PENDING;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "photo",fetch = FetchType.LAZY)
    private Collection<File> files;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "categories_photos",
            joinColumns = @JoinColumn(name = "photo_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id",referencedColumnName = "id")
    )
    private Collection<Category> categories;

    @OneToMany(mappedBy = "photo",fetch = FetchType.LAZY)
    private Collection<OrderDetails> orderDetails;

}
