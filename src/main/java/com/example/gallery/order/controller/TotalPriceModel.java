package com.example.gallery.order.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TotalPriceModel {
    private Integer amount=0;
}
