package com.example.gallery.order.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.base.config.PhotoConfig;
import com.example.gallery.order.service.OrderDetailsService;
import com.example.gallery.order.service.OrderDetailsServiceImpl;
import com.example.gallery.order.service.OrderService;
import com.example.gallery.photo.controller.PhotoModel;
import com.example.gallery.photo.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/order/details")
public class OrderDetailsControllerAnyRole {
    private OrderDetailsService orderDetailsService;

    @Autowired
    public OrderDetailsControllerAnyRole(OrderDetailsService orderDetailsService) {
        this.orderDetailsService = orderDetailsService;
    }

    @GetMapping(value = {"/index","/index/"})
    public ResponseEntity<?> index() throws Exception {
        List<OrderDetailsModel> orderDetailsModels = orderDetailsService.getOpenOrderDetails();
        TotalPriceModel totalPriceModel = OrderDetailsServiceImpl.totalPrice(orderDetailsModels);
        for (int i =0;i<orderDetailsModels.size();i++){
            orderDetailsModels.get(i).getFiles().removeIf(file->!file.getWidth().equals(PhotoConfig.widths[0]));
        }
        return ResponseEntity.ok(new ApiResponse(true,new OrderDetailsResponseIndex(orderDetailsModels,totalPriceModel),"","", HttpStatus.OK));
    }
}
