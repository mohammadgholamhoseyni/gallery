package com.example.gallery.category.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.category.persistence.Category;
import com.example.gallery.category.service.CategoryService;
import com.example.gallery.category.service.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = {"/",""})
    public ResponseEntity<?> index() throws Exception {
        List<CategoryModel> categoryModels = categoryService.index();
        return ResponseEntity.ok(new ApiResponse(true,categoryModels,"","", HttpStatus.OK));
    }
    @GetMapping(value = {"/tree","/tree/"})
    public ResponseEntity<?> tree() throws Exception {
        List<CategoryTreeChildModel> categories = categoryService.tree();
        return ResponseEntity.ok(new ApiResponse(true,categories,"","", HttpStatus.OK));
    }
}
