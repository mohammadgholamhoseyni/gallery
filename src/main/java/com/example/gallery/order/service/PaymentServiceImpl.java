package com.example.gallery.order.service;

import com.example.gallery.base.Exceptions.ResourceNotFoundException;
import com.example.gallery.order.persistence.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Qualifier("PaymentServiceImpl")
@Transactional(rollbackOn = Exception.class)
public class PaymentServiceImpl implements PaymentService {

    private PaymentRepository paymentRepository;
    private OrderRepository orderRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository, OrderRepository orderRepository) {
        this.paymentRepository = paymentRepository;
        this.orderRepository = orderRepository;
    }


    @Override
    public void create(Integer orderId, PaymentStatusEnum status) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
        Payment payment = new Payment();
        payment.setAuthority("10000001");
        payment.setStatus(status);
        payment.setRefId("1234");
        payment.setOrder(order);
        paymentRepository.save(payment);
    }
}
