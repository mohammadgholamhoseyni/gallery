package com.example.gallery.order.persistence;

import com.example.gallery.base.Entity.DateAudit;
import com.example.gallery.base.config.SchemaConfig;
import com.example.gallery.photo.persistence.Photo;
import com.example.gallery.user.persistence.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "orders",schema = SchemaConfig.DB)
public class Order extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "amount",nullable = false)
    private Integer amount;

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private OrderStatusEnum status=OrderStatusEnum.OPENED;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "order",fetch = FetchType.LAZY)
    private Collection<OrderDetails> orderDetails;

    @OneToOne(mappedBy = "order",fetch = FetchType.LAZY)
    private Payment payment;
}
