package com.example.gallery.order.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.order.persistence.Order;
import com.example.gallery.order.persistence.OrderStatusEnum;
import com.example.gallery.order.persistence.PaymentStatusEnum;
import com.example.gallery.order.service.OrderDetailsService;
import com.example.gallery.order.service.OrderService;
import com.example.gallery.order.service.PaymentService;
import com.example.gallery.order.service.PaymentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/payment")
public class PaymentControllerAnyRole {
    private PaymentService paymentService;
    private OrderService orderService;


    @Autowired
    public PaymentControllerAnyRole(PaymentService paymentService, OrderService orderService) {
        this.paymentService = paymentService;
        this.orderService = orderService;
    }

    @PutMapping(value = {"/do", "/do/"})
    public ResponseEntity<?> doPayment() throws Exception {
        OrderModel orderModel = orderService.checkOpenOrder();
        if (orderModel == null) {
            return ResponseEntity.ok(new ApiResponse(false, "", "سفارشات شما خالی است", "", HttpStatus.OK));
        }
        OrderModel updateOrder = orderService.updateAmountAndStatus(orderModel);
        PaymentUtil paymentUtil = new PaymentUtil(updateOrder.getAmount(), updateOrder.getId());
        paymentUtil.doPayment();
        return ResponseEntity.ok(new ApiResponse(true, paymentUtil.toString(), "انتقال به درگاه پرداخت", "", HttpStatus.OK));
    }

    @PutMapping(value = {"/accept/{orderId}", "/accept/{orderId}"})
    public ResponseEntity<?> acceptPayment(@PathVariable Integer orderId) throws Exception {
        paymentService.create(orderId, PaymentStatusEnum.SUCCESS);
        orderService.changeStatus(orderId, OrderStatusEnum.ACCEPTED);
        return ResponseEntity.ok(new ApiResponse(true, "", "پرداخت موفقیت آمیز بود.", "", HttpStatus.OK));
    }
    @PutMapping(value = {"/reject/{orderId}", "/reject/{orderId}"})
    public ResponseEntity<?> rejectPayment(@PathVariable Integer orderId) throws Exception {
        paymentService.create(orderId,PaymentStatusEnum.FAILED);
        orderService.changeStatus(orderId,OrderStatusEnum.REJECTED);
        return ResponseEntity.ok(new ApiResponse(false, "", "پرداخت ناموفق بود.", "", HttpStatus.OK));
    }
}
