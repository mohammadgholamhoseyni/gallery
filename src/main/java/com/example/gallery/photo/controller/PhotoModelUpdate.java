package com.example.gallery.photo.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PhotoModelUpdate {
    private Integer id;

    private String originalName;

    private Integer price;

    private List<Integer> categories_id;
}
