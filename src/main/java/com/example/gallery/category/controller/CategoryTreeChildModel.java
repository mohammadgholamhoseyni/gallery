package com.example.gallery.category.controller;

import lombok.Data;

import java.util.List;

@Data
public class CategoryTreeChildModel {
    private Integer id;
    private String name;
    private List<CategoryTreeChildModel> child;
}
