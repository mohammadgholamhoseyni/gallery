package com.example.gallery.base.config;

public class PaginateConfig {
    public static Integer PAGE_SIZE = 20;
    public static String SORT_PAGE_BY = "id";
}
