package com.example.gallery.photo.persistence;

public enum PhotoAdminVerifiedEnum {
    PENDING,ACCEPTED,REJECTED
}
