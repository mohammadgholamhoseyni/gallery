package com.example.gallery.user.controller;

import com.example.gallery.base.Responses.ApiResponse;
import com.example.gallery.base.config.RoleConfig;
import com.example.gallery.order.service.OrderService;
import com.example.gallery.photo.service.PhotoService;
import com.example.gallery.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("admin/user")
public class UserControllerAdmin {

    private UserService userService;
    private PhotoService photoService;
    private OrderService orderService;

    @Autowired
    public UserControllerAdmin(UserService userService, PhotoService photoService, OrderService orderService) {
        this.userService = userService;
        this.photoService = photoService;
        this.orderService = orderService;
    }

    @GetMapping(value = "/index")
    public ResponseEntity<?> index(@RequestParam(defaultValue = "0") Integer pageNo) throws Exception {
        List<UserModel> userModels = userService.index(pageNo);
        return ResponseEntity.ok(new ApiResponse(true,userModels,"","", HttpStatus.OK));
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> create(@Valid @RequestBody UserModelCreate userModelCreate) throws Exception {
        UserModel userModel = userService.create(userModelCreate, RoleConfig.ROLE_ADMIN);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"با موفقیت اضاغه شدید.","", HttpStatus.OK));
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@Valid @RequestBody UserModelUpdate userModelUpdate) throws Exception {
        UserModel userModel = userService.update(userModelUpdate);
        return ResponseEntity.ok(new ApiResponse(true,userModel,"با موفقیت ویرایش گردید","", HttpStatus.OK));
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) throws Exception {
        if (photoService.checkOtherUserHasPhoto(id))
            return ResponseEntity.ok(new ApiResponse(false,"","ابتدا عکس های آپلود شده ی  را حذف نمایید","", HttpStatus.OK));
        if (orderService.checkOtherUserHasOrder(id))
            return ResponseEntity.ok(new ApiResponse(false,""," سفارش ثبت شده موجود است","", HttpStatus.OK));
        userService.delete(id);
        return ResponseEntity.ok(new ApiResponse(true,"","با موفقیت حذف شد","", HttpStatus.OK));
    }


}
